#!/usr/bin/env python3

# Copyright (C) 2022 Sylvain Munaut and Harald Welte
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

from ice40usbtrace import ICE40USBTrace
if __name__ == '__main__':
    ut = ICE40USBTrace()
    ut.start()
    ut.run()
