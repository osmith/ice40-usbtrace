#!/usr/bin/env python3

# Copyright (C) 2022 Sylvain Munaut and Harald Welte
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

import abc
from ice40usbtrace import ICE40USBTrace, USBPacketHandler
from ice40usbtrace.transfer import *

if __name__ == '__main__':
    # Create an instance of the USB transfer handler that prints each transfer
    transfer_handler = USBTransferHandlerPrint()
    # Create an instance of the USB packet handler that re-combines transfers
    packet_handler = TransferCombiner(transfer_handler = transfer_handler)
    # Create the tracer instance with the packet handler for re-combining
    ut = ICE40USBTrace(packet_handler = packet_handler)
    ut.start()
    ut.run()
