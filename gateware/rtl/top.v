/*
 * top.v
 *
 * vim: ts=4 sw=4
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: CERN-OHL-P-2.0
 */

`default_nettype none

module top (
	// USP tap
	input  wire        tap_dp,
	input  wire        tap_dn,

	// USB
	inout  wire        usb_dp,
	inout  wire        usb_dn,
	output wire        usb_pu,

	// Flash & PSRAM
	output wire        spi_sck,
	inout  wire  [3:0] spi_io,
	output wire  [1:0] spi_cs_n,

	// UART
	input  wire        uart_rx,
	output wire        uart_tx,

	// Clock
	input  wire        clk_in,

	// Button
	input  wire        btn,

	// RGB LEDs
	output wire  [2:0] rgb
);

	localparam integer WN = 7;
	genvar i;


	// Signals
	// -------

	// Wishbone
	wire   [15:0] wb_addr;
	wire   [31:0] wb_rdata [0:WN-1];
	wire   [31:0] wb_wdata;
	wire    [3:0] wb_wmsk;
	wire [WN-1:0] wb_cyc;
	wire          wb_we;
	wire [WN-1:0] wb_ack;

	wire [(32*WN)-1:0] wb_rdata_flat;

	// Misc/Platform
	reg         misc_bus_ack;
	reg         misc_bus_we_csr;
	reg         misc_boot_now;
	reg   [1:0] misc_boot_sel;

	// Memory interface
	wire [23:0] mi_addr;
	wire [ 6:0] mi_len;
	wire        mi_rw;
	wire        mi_valid;
	wire        mi_ready;

	wire  [7:0] mi_wdata;
	wire        mi_wack;
	wire        mi_wlast;

	wire  [7:0] mi_rdata;
	wire        mi_rstb;
	wire        mi_rlast;

	// QPI PHY
	wire  [7:0] qpi_phy_io_i;
	wire  [7:0] qpi_phy_io_o;
	wire  [3:0] qpi_phy_io_oe;
	wire  [1:0] qpi_phy_clk_o;
	wire  [1:0] qpi_phy_cs_o;

	// DMA
	wire        dma_req;
	wire        dma_gnt;
	wire [15:0] dma_addr;
	wire [31:0] dma_data;
	wire        dma_we;

	// USB Core
		// Wishbone in 48 MHz domain
	wire [11:0] ub_addr;
	wire [15:0] ub_wdata;
	wire [15:0] ub_rdata;
	wire        ub_cyc;
	wire        ub_we;
	wire        ub_ack;

		// EP Buffer
	wire [ 8:0] ep_tx_addr_0;
	wire [31:0] ep_tx_data_0;
	wire        ep_tx_we_0;

	wire [ 8:0] ep_rx_addr_0;
	wire [31:0] ep_rx_data_1;
	wire        ep_rx_re_0;

	// Clock / Reset
	wire        clk_1x;
	wire        clk_2x;
	wire        rst;


	// SoC
	// ---

	soc_picorv32_base #(
		.WB_N     (WN),
		.WB_DW    (32),
		.WB_AW    (16),
		.BRAM_AW  (8), //  1k BRAM
		.SPRAM_AW (14) // 64k SPRAM
	) base_I (
		.wb_addr  (wb_addr),
		.wb_rdata (wb_rdata_flat),
		.wb_wdata (wb_wdata),
		.wb_wmsk  (wb_wmsk),
		.wb_we    (wb_we),
		.wb_cyc   (wb_cyc),
		.wb_ack   (wb_ack),
		.clk      (clk_1x),
		.rst      (rst)
	);

	for (i=0; i<WN; i=i+1)
		assign wb_rdata_flat[i*32+:32] = wb_rdata[i];


	// Misc [0]
	// ----

	// DFU helper
	dfu_helper #(
		.SAMP_TW    ( 7),
		.LONG_TW    (19),
		.BTN_MODE   ( 3)
	) dfu_I (
		.boot_sel  (misc_boot_sel),
		.boot_now  (misc_boot_now),
		.btn_in    (btn),
		.btn_tick  (1'b0),
		.btn_val   (),
		.btn_press (),
		.clk       (clk_1x),
		.rst       (rst)
	);

	// Bus interface
		// Ack
	always @(posedge clk_1x)
		misc_bus_ack <= wb_cyc[0] & ~misc_bus_ack;

	assign wb_ack[0] = misc_bus_ack;

		// Read Data
	assign wb_rdata[0] = 32'h00000000;

		// Writes
	always @(posedge clk_1x)
		misc_bus_we_csr <= wb_cyc[0] & ~misc_bus_ack & wb_we;

		// Bootloader
	always @(posedge clk_1x)
		if (rst)
			{ misc_boot_now, misc_boot_sel } <= 3'b000;
		else
			{ misc_boot_now, misc_boot_sel } <= misc_bus_we_csr ? wb_wdata[2:0] : 3'b000;


	// UART [1]
	// ----

	uart_wb #(
		.DIV_WIDTH(12),
		.DW(32)
	) uart_I (
		.uart_tx  (uart_tx),
		.uart_rx  (uart_rx),
		.wb_addr  (wb_addr[1:0]),
		.wb_rdata (wb_rdata[1]),
		.wb_we    (wb_we),
		.wb_wdata (wb_wdata),
		.wb_cyc   (wb_cyc[1]),
		.wb_ack   (wb_ack[1]),
		.clk      (clk_1x),
		.rst      (rst)
	);


	// RGB LEDs [2]
	// --------

	ice40_rgb_wb #(
		.CURRENT_MODE("0b1"),
		.RGB0_CURRENT("0b000001"),
		.RGB1_CURRENT("0b000001"),
		.RGB2_CURRENT("0b000001")
	) rgb_I (
		.pad_rgb    (rgb),
		.wb_addr    (wb_addr[4:0]),
		.wb_rdata   (wb_rdata[2]),
		.wb_wdata   (wb_wdata),
		.wb_we      (wb_we),
		.wb_cyc     (wb_cyc[2]),
		.wb_ack     (wb_ack[2]),
		.clk        (clk_1x),
		.rst        (rst)
	);


	// QPI controller [3]
	// --------------

	// Controller
	qpi_memctrl #(
		.CMD_READ   (16'hEBEB),
		.CMD_WRITE  (16'h0202),
		.DUMMY_CLK  (6),
		.PAUSE_CLK  (8),
		.FIFO_DEPTH (1),
		.N_CS       (2),
		.DATA_WIDTH (8),
		.PHY_SPEED  (2),
		.PHY_WIDTH  (1),
		.PHY_DELAY  (3)
	) memctrl_I (
		.phy_io_i   (qpi_phy_io_i),
		.phy_io_o   (qpi_phy_io_o),
		.phy_io_oe  (qpi_phy_io_oe),
		.phy_clk_o  (qpi_phy_clk_o),
		.phy_cs_o   (qpi_phy_cs_o),
		.mi_addr_cs (2'b01),
		.mi_addr    (mi_addr),
		.mi_len     (mi_len),
		.mi_rw      (mi_rw),
		.mi_valid   (mi_valid),
		.mi_ready   (mi_ready),
		.mi_wdata   (mi_wdata),
		.mi_wack    (mi_wack),
		.mi_wlast   (mi_wlast),
		.mi_rdata   (mi_rdata),
		.mi_rstb    (mi_rstb),
		.mi_rlast   (mi_rlast),
		.wb_wdata   (wb_wdata),
		.wb_rdata   (wb_rdata[3]),
		.wb_addr    (wb_addr[4:0]),
		.wb_we      (wb_we),
		.wb_cyc     (wb_cyc[3]),
		.wb_ack     (wb_ack[3]),
		.clk        (clk_1x),
		.rst        (rst)
	);

	// PHY
	qpi_phy_ice40_2x #(
		.N_CS     (2),
		.WITH_CLK (1),
	) phy_I (
		.pad_io    (spi_io),
		.pad_clk   (spi_sck),
		.pad_cs_n  (spi_cs_n),
		.phy_io_i  (qpi_phy_io_i),
		.phy_io_o  (qpi_phy_io_o),
		.phy_io_oe (qpi_phy_io_oe),
		.phy_clk_o (qpi_phy_clk_o),
		.phy_cs_o  (qpi_phy_cs_o),
		.clk_1x    (clk_1x),
		.clk_2x    (clk_2x)
	);


	// USB Buffer [4]
	// ----------

	soc_usb_buf_bridge usb_buf_I (
		.wb_addr      (wb_addr),
		.wb_rdata     (wb_rdata[4]),
		.wb_wdata     (wb_wdata),
		.wb_wmsk      (wb_wmsk),
		.wb_we        (wb_we),
		.wb_cyc       (wb_cyc[4]),
		.wb_ack       (wb_ack[4]),
		.dma_req      (dma_req),
		.dma_gnt      (dma_gnt),
		.dma_addr     (dma_addr),
		.dma_data     (dma_data),
		.dma_we       (dma_we),
		.ep_tx_addr_0 (ep_tx_addr_0),
		.ep_tx_data_0 (ep_tx_data_0),
		.ep_tx_we_0   (ep_tx_we_0),
		.ep_rx_addr_0 (ep_rx_addr_0),
		.ep_rx_data_1 (ep_rx_data_1),
		.ep_rx_re_0   (ep_rx_re_0),
		.clk          (clk_1x),
		.rst          (rst)
	);


	// USB core [5]
	// --------

	// Cross-clock
	xclk_wb #(
		.DW(16),
		.AW(12)
	)  wb_48m_xclk_I (
		.s_addr  (wb_addr[11:0]),
		.s_wdata (wb_wdata[15:0]),
		.s_rdata (wb_rdata[5][15:0]),
		.s_cyc   (wb_cyc[5]),
		.s_ack   (wb_ack[5]),
		.s_we    (wb_we),
		.s_clk   (clk_1x),
		.m_addr  (ub_addr),
		.m_wdata (ub_wdata),
		.m_rdata (ub_rdata),
		.m_cyc   (ub_cyc),
		.m_ack   (ub_ack),
		.m_we    (ub_we),
		.m_clk   (clk_2x),
		.rst     (rst)
	);

	assign wb_rdata[5][31:16] = 0;

	// Core
	usb #(
		.EPDW(32)
	) usb_I (
		.pad_dp       (usb_dp),
		.pad_dn       (usb_dn),
		.pad_pu       (usb_pu),
		.ep_tx_addr_0 (ep_tx_addr_0),
		.ep_tx_data_0 (ep_tx_data_0),
		.ep_tx_we_0   (ep_tx_we_0),
		.ep_rx_addr_0 (ep_rx_addr_0),
		.ep_rx_data_1 (ep_rx_data_1),
		.ep_rx_re_0   (ep_rx_re_0),
		.ep_clk       (clk_1x),
		.wb_addr      (ub_addr),
		.wb_rdata     (ub_rdata),
		.wb_wdata     (ub_wdata),
		.wb_we        (ub_we),
		.wb_cyc       (ub_cyc),
		.wb_ack       (ub_ack),
		.clk          (clk_2x),
		.rst          (rst)
	);


	// USB Tap [6]
	// -------

	ut_top ut_I (
		.tap_dp   (tap_dp),
		.tap_dn   (tap_dn),
		.mi_addr  (mi_addr),
		.mi_len   (mi_len),
		.mi_rw    (mi_rw),
		.mi_valid (mi_valid),
		.mi_ready (mi_ready),
		.mi_wdata (mi_wdata),
		.mi_wack  (mi_wack),
		.mi_wlast (mi_wlast),
		.mi_rdata (mi_rdata),
		.mi_rstb  (mi_rstb),
		.mi_rlast (mi_rlast),
		.dma_req  (dma_req),
		.dma_gnt  (dma_gnt),
		.dma_addr (dma_addr),
		.dma_data (dma_data),
		.dma_we   (dma_we),
		.wb_addr  (wb_addr[3:0]),
		.wb_rdata (wb_rdata[6]),
		.wb_wdata (wb_wdata),
		.wb_we    (wb_we),
		.wb_cyc   (wb_cyc[6]),
		.wb_ack   (wb_ack[6]),
		.clk_1x   (clk_1x),
		.clk_2x   (clk_2x),
		.rst      (rst)
	);


	// Clock/Reset Generation
	// ----------------------

	sysmgr sysmgr_I (
		.clk_in  (clk_in),
		.clk_1x  (clk_1x),
		.clk_2x  (clk_2x),
		.rst     (rst)
	);

endmodule // top
