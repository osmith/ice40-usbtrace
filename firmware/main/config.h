/*
 * config.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define MISC_BASE	0x80000000
#define UART_BASE	0x81000000
#define LED_BASE	0x82000000
#define QPI_BASE	0x83000000
#define USB_DATA_BASE	0x84000000
#define USB_CORE_BASE	0x85000000
#define TAP_BASE	0x86000000

#define SYS_CLK_FREQ	24000000
