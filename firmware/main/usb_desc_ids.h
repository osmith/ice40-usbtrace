/*
 * usb_desc_ids.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#define USB_INTF_TAP		0
#define USB_INTF_DFU		1
#define USB_INTF_NUM		2

#define USB_EP_TAP_IN		0x81
#define USB_EP_TAP_INTR		0x82
