/*
 * usb_tap_proto.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: MIT
 *
 * Header file describing the USB protocol used by ice40-usbtrace
 */

#pragma once

/***********************************************************************
 * Control Endpoint / USB Tap Interface Requests
 ***********************************************************************/

#define IUT_INTF_CAPTURE_STATUS		0x10	/*!< struct iut_capture_status */
#define IUT_INTF_CAPTURE_START		0x12
#define IUT_INTF_CAPTURE_STOP		0x13
#define IUT_INTF_BUFFER_GET_LEVEL	0x20	/*!< uint32_t bytes */
#define IUT_INTF_BUFFER_FLUSH		0x21

struct iut_capture_status {
	uint8_t  state;		/*!< enum iut_capture_status */
	uint8_t  flags;		/*!< enum iut_capture_flags  */
	uint32_t level;
} __attribute__((packed));

enum iut_capture_state {
	IUT_CAP_STATE_STOPPED	= 0,
	IUT_CAP_STATE_RUNNING	= 1,
	IUT_CAP_STATE_FLUSHING	= 2,
};

enum iut_capture_flags {
	IUT_CAP_FLAGS_OVERFLOW	= (1 << 0),	/* Cleared on start */
};
