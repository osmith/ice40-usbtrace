/*
 * usb_tap.h
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

void usb_tap_reset(void);
int  usb_tap_start(void);
int  usb_tap_stop(void);
int  usb_tap_flush(void);

void usb_tap_debug(void);

void usb_tap_poll(void);
void usb_tap_init(void);
