/*
 * misc.c
 *
 * Copyright (C) 2019-2020  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdbool.h>
#include <stdint.h>

#include "config.h"
#include "misc.h"


struct wb_misc {
	uint32_t warmboot;
} __attribute__((packed,aligned(4)));

static volatile struct wb_misc * const misc_regs = (void*)(MISC_BASE);


void
reboot(int fw)
{
	misc_regs->warmboot = (1 << 2) | (fw << 0);
}
