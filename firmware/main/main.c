/*
 * main.c
 *
 * Copyright (C) 2022  Sylvain Munaut <tnt@246tNt.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <no2usb/usb.h>
#include <no2usb/usb_dfu_rt.h>

#include "console.h"
#include "led.h"
#include "misc.h"
#include "spi.h"
#include "usb_tap.h"
#include "utils.h"

extern const struct usb_stack_descriptors app_stack_desc;

static const char *fw_build_str = BUILD_INFO;


static void
serial_no_init()
{
	uint8_t buf[8];
	char *id, *desc;
	int i;

	flash_manuf_id(buf);
	printf("Flash Manufacturer : %s\n", hexstr(buf, 3, true));

	flash_unique_id(buf);
	printf("Flash Unique ID    : %s\n", hexstr(buf, 8, true));

	/* Overwrite descriptor string */
		/* In theory in rodata ... but nothing is ro here */
	id = hexstr(buf, 8, false);
	desc = (char*)app_stack_desc.str[1];
	for (i=0; i<16; i++)
		desc[2 + (i << 1)] = id[i];
}

static void
boot_dfu(void)
{
	/* Force re-enumeration */
	usb_disconnect();

	/* Boot firmware */
	reboot(1);
}

void
usb_dfu_rt_cb_reboot(void)
{
        boot_dfu();
}


void main()
{
	int cmd = 0;

	/* Clear any LED state */
	led_init();

	/* Init console IO */
	console_init();
	printf("\n\nBooting %s\n", fw_build_str);

	/* SPI */
	spi_init();
	serial_no_init();

	/* Enable USB directly */
	usb_init(&app_stack_desc);
	usb_dfu_rt_init();
	usb_tap_init();
	usb_connect();

	/* Main loop */
	while (1)
	{
		/* Prompt ? */
		if (cmd >= 0)
			printf("Command> ");

		/* Poll for command */
		cmd = getchar_nowait();

		if (cmd >= 0) {
			if (cmd > 32 && cmd < 127)
				putchar(cmd);
			putchar('\r');
			putchar('\n');

			switch (cmd)
			{
			case 'b':
				boot_dfu();
				break;
			case 'p':
				panic("Test panic");
				break;
			case 'c':
				usb_connect();
				break;
			case 'd':
				usb_disconnect();
				break;
			case 'u':
				usb_debug_print();
				usb_debug_print_ep(1,1);
				break;
			case 's':
				usb_tap_start();
				break;
			case 'S':
				usb_tap_stop();
				break;
			case 'a':
				usb_tap_debug();
				break;
			case 'f':
				usb_tap_flush();
				break;
			default:
				break;
			}
		}

		/* USB poll */
		usb_poll();
		usb_tap_poll();
	}
}
